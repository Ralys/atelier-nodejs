$(document).ready(function() {

    /* Éléments du DOM */
    var $modalConnection = $("#modal-connection"),
        $usersList = $("ul.nav.chat"),
        $loggedInAs = $("#chat-loggedin-as"),
        $chatWindow = $("#chat-window"),
        $chatWrapper = $chatWindow.closest('.panel-body'),
        $chatForm = $("form#chat-form"),
        $inputMessage = $chatForm.find("input#message");

    /* Là où la magie opère */
    client(onConnection, onSendMessage, connectionError, connectionAllowed,
           addUser, removeUser, showPublicMessage, showPrivateMessage);

    /* Fonctions de manipulation du DOM */
    $modalConnection.on('shown.bs.modal', function(e) {
        $(this).find('input').first().focus();
    }).modal('show');

    $(document).on('click', '.exit', function(e) {
        location.reload();
    });

    $(document).on('click', '.insert-element', function(e) {
        e.preventDefault();

        var element = $(this).data('element');
        insertElement(element + ' ');
    });

    function insertElement(element) {
        var content = $inputMessage.val(),
            separator = content.length == 0 ? '' : ' ';

        $inputMessage.val(content + separator + element).focus();
    }

    function onConnection(callback) {
        $modalConnection.find("form#chat-connection").on('submit', function(e) {
            e.preventDefault();
            callback($(this).serializeObject());
        });
    }

    function connectionError(error) {
        var $form = $modalConnection.find("form");

        /* Purge les erreurs précédentes */
        $form.find('.form-group').removeClass('has-feedback')
                                 .removeClass('has-error');
        $form.find('.help-block').remove();
        $form.find('.glyphicon.glyphicon-remove.form-control-feedback').remove();

        /* On ajoute l'erreur présente */
        var $formGroup = $form.find('#'+error.name).closest('.form-group');
        $formGroup.addClass('has-feedback')
                  .addClass('has-error');

        $formGroup.append('<span class="help-block">' + error.message + '</span>')
        $formGroup.append('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
    }

    function connectionAllowed(response) {
        for(var username in response.others) {
            addUserInList(response.others[username]);
        }

        $loggedInAs.html("Connecté en temps que " + response.self.name + " (<strong>@" + response.self.pseudo + "</strong>)");
        $modalConnection.modal('hide');
        $inputMessage.focus();
    }

    function wrapInsertElement(toWrap, element) {
        element = element || toWrap;
        return '<a class="insert-element" data-element="' + element + '" href="#" title="' + element + '">'+ toWrap + '</a>';
    }

    function wrapPseudo(pseudo) {
        return wrapInsertElement('@'+pseudo);
    }

    function addUser(user) {
        addUserInList(user);
        addUserInChat(user);
    }

    function removeUser(user) {
        removeUserInList(user);
        removeUserInChat(user);
    }

    function slide() {
        var height = $chatWindow.height();
        $chatWrapper.animate({scrollTop: height}, 250);
    }

    function addUserInList(user) {
        var render = '<div class="col-lg-3"><div class="img-circle" title="Avatar de '+ user.name +'">' + user.name[0] + '</div></div>' +
                     '<div class="col-lg-9" style="padding-top:5px">'+
                        '<strong class="primary-font">' + user.name + '</strong><br>'+
                        '<small class="text-muted">@' + user.pseudo + '</small>'+
                     '</div>';

        $usersList.append('<li class="user" id="' + user.pseudo + '"><div class="row">' + wrapInsertElement(render, '@'+user.pseudo) + '</div></li>');
    }

    function addUserInChat(user) {
        var render = '<li class="text-center">'+
            '<em>' + user.name + ' ('+ wrapPseudo(user.pseudo) + ') vient de rejoindre le chat.' + '</em>'+
        '</li>';

        appendToChat(render);
    }

    function removeUserInList(user) {
        $usersList.find('#'+user.pseudo).remove();
    }

    function removeUserInChat(user) {
        var render = '<li class="text-center">'+
            '<em>' + user.name + ' ('+ wrapPseudo(user.pseudo) + ') vient de quitter le chat.' + '</em>'+
        '</li>';

        appendToChat(render);
    }

    function onSendMessage(callback) {
        $chatForm.on('submit', function(e) {
            e.preventDefault();

            var content = $inputMessage.val().trim();
            if(content.length == 0) return;

            callback(content);
            $inputMessage.val('').focus();
        });
    }

    function showPublicMessage(message) {
        showMessage(message);
    }

    function showPrivateMessage(message) {
        showMessage(message, true);
    }

    function appendToChat(element) {
        $chatWindow.append(element);
        slide();
    }

    function showMessage(message, isPrivate) {
        var render = '<li class="left clearfix ' + (isPrivate ? 'private':'') + '">' +
            '<span class="chat-img pull-left">' +
                '<div class="img-circle" title="Avatar de '+ message.author.name +'">' + message.author.name[0] + '</div>' +
            '</span>'+
            '<div class="chat-body clearfix">' +
                '<div class="header">' +
                    wrapInsertElement('<strong class="primary-font">' + message.author.name + '</strong> <small class="text-muted">@' + message.author.pseudo + '</small>' +
                    '<small class="pull-right text-muted">' +
                        '<i class="fa fa-clock-o fa-fw"></i>' + '<span class="date moment" data-date="' + message.date + '">'+moment(message.date).fromNow() + '</span>' +
                    '</small>', '@'+message.author.pseudo) +
                '</div>'+
                '<p>' + parseMessage(message.content) + '</p>'+
            '</div>'+
        '</li>';

        appendToChat(render);
    }

    var replacements = [
        {
            input: new RegExp("^\/cookie", 'g'),
            output: '<img src="./assets/images/eastereggs/cookie.gif" alt="Dancing cookie" title="Dancing cookie"><br>'
        },

        {
            input: new RegExp("(http(s)?://.+)", 'g'),
            output: '<a href="$1" target="_blank">$1</a>'
        },

        {
            input: new RegExp("@([A-Za-z0-9_\-]+)", 'g'),
            output: wrapPseudo('$1')
        },

        {
            input: new RegExp("\\:\\)", 'g'),
            output: '<span class="emoticon smile"></span>'
        },

        {
            input: new RegExp("\\:D", 'g'),
            output: '<span class="emoticon big-smile"></span>'
        },

        {
            input: new RegExp("\\:\\(", 'g'),
            output: '<span class="emoticon sad"></span>'
        },

        {
            input: new RegExp("\\:&apos;\\(", 'g'),
            output: '<span class="emoticon cry"></span>'
        },

        {
            input: new RegExp("\\:P", 'g'),
            output: '<span class="emoticon tongue"></span>'
        },

        {
            input: new RegExp("\\;\\)", 'g'),
            output: '<span class="emoticon wink"></span>'
        },

        {
            input: new RegExp("\\:\\*", 'g'),
            output: '<span class="emoticon kiss"></span>'
        },

        {
            input: new RegExp("&lt;3", 'g'),
            output: '<span class="emoticon heart"></span>'
        },

        {
            input: new RegExp("8\\|", 'g'),
            output: '<span class="emoticon sunglasses"></span>'
        },

        {
            input: new RegExp("\\:-/", 'g'),
            output: '<span class="emoticon unsure"></span>'
        },

        {
            input: new RegExp("\\:3", 'g'),
            output: '<span class="emoticon curly-lips"></span>'
        },

        {
            input: new RegExp("o\\.O", 'g'),
            output: '<span class="emoticon wtf"></span>'
        }
    ];

    function parseMessage(content) {
        var result = content;

        replacements.forEach(function(current) {
            result = result.replace(current.input, current.output);
        });

        return result;
    }

    var emoticons = [
        { element: ":)",    image: '<span class="emoticon smile"></span>' },
        { element: ":D",    image: '<span class="emoticon big-smile"></span>' },
        { element: ":(",    image: '<span class="emoticon sad"></span>' },
        { element: ":'('",  image: '<span class="emoticon cry"></span>' },
        { element: "<3",    image: '<span class="emoticon heart"></span>' },
        { element: ";)",    image: '<span class="emoticon wink"></span>' },
        { element: ":*",    image: '<span class="emoticon kiss"></span>' },
        { element: "8|",    image: '<span class="emoticon sunglasses"></span>' },
        { element: ":-/",    image: '<span class="emoticon unsure"></span>' },
        { element: ":3",    image: '<span class="emoticon curly-lips"></span>' },
        { element: "o.O",   image: '<span class="emoticon wtf"></span>' },
        { element: ":P",    image: '<span class="emoticon tongue"></span>' },
    ];

    (function showEmoticons() {
        var render = '';

        emoticons.forEach(function(emoticon) {
            render += '<div class="col-lg-3">' + wrapInsertElement(emoticon.image, emoticon.element) + '</div>';
        });

        $('.dropdown-menu').html('<div class="row"><div class="col-lg-12">'+render+'</div></div>');
    })();

    (function showDates() {
        var $dates = $(".date.moment");
        $dates.each(function() {
            var date = $(this).data('date'),
                fromNowFormat = moment(date).fromNow();

            $(this).text(fromNowFormat);
        });

        setTimeout(showDates, 60*1000);
    })();
});