global.__base = __dirname;

var express       = require('express'),
    io            = require('socket.io'),
    ChatServer    = require(__base+'/controllers/ChatServer');

var config = {
    dev: true,
    port: 1337
};

/* Création de l'application */
var app = express();

/* Fichiers statiques */
app.use(express.static('public'));

/* Démarrer le serveur */
var server = app.listen(config.port, function() {
    var port = server.address().port;
    console.log(`Chat avec Node.js disponible à l'adresse : http://localhost:${port}`);
});

/* Implémentation des comportements lors des événements */
var cs = new ChatServer(io(server));