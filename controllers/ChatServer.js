var HTMLEntities = require('html-entities').AllHtmlEntities;

var User = require(__base+'/models/User'),
    PseudoError = require(__base+'/models/errors/PseudoError'),
    Message = require(__base+'/models/Message');

module.exports = function(io) {
    

    function broadcast() {
        io.emit.apply(io, arguments);
    }

    function getAllUsers(users, self) {
        var allUsers = [];

        for(var username in users) {
            if(username != self.pseudo) {
                allUsers.push(users[username].user);
            }
        }

        return allUsers;
    }
};