var PseudoError = require(__base+'/models/errors/PseudoError'),
    NameError   = require(__base+'/models/errors/NameError');

const regexPseudo = /^[A-Za-z0-9][A-Za-z0-9_\-]+$/;

function User(pseudo, name) {
    if(!pseudo)
        throw new PseudoError("Le pseudonyme doit être renseigné");

    if(!name)
        throw new NameError("Le nom doit être renseigné");

    if(!regexPseudo.test(pseudo))
        throw new PseudoError('Le pseudonyme doit commencer par un caractère alphanumérique.' +
                              ' Il peut comporter les caractères alphanumériques, "-" et "_"');

    this.pseudo = pseudo;
    this.name   = name;
}

module.exports = User;