const detectPseudo = /@([A-Za-z0-9_\-]+)/;

function Message(user, content) {
    this.author = user;
    this.content = content;
    this.date = new Date();

    this.parse();
}

Message.prototype.parse = function() {
    var result = this.content.match(detectPseudo);
    this.target = (result) ? result[1] : null;
};

Message.prototype.isPrivate = function() {
    return (this.target != null);
};

module.exports = Message;