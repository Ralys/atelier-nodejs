function NameError(message) {
    this.name = "name";
    this.message = message;
}

NameError.prototype = Object.create(Error.prototype);
NameError.prototype.constructor = NameError;

module.exports = NameError;