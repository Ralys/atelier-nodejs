function PseudoError(message) {
    this.name = "pseudo";
    this.message = message;
}

PseudoError.prototype = Object.create(Error.prototype);
PseudoError.prototype.constructor = PseudoError;

module.exports = PseudoError;